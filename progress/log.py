import datetime
import time
from pymongo import MongoClient


class MongoProgressLogger(object):
    def __init__(self, host, title):
        self._id = title + '.' + time.strftime('%Y_%m_%d.%H_%M_%S_')
        self._db = MongoClient(host).progress
        self._eIter = -1
        self._date = datetime.datetime.utcnow()

    def entry(self, collection, o, optional):
        self._eIter += 1
        d = {
            "_id": self._id + str(self._eIter),
            "o": o,
            "at": self._date
        }

        for index, w in enumerate(optional):
            d["p" + str(index)] = w

        self._db[collection].insert(d)


