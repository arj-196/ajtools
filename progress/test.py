import log

if __name__ == "__main__":
    logger = log.MongoProgressLogger("localhost:27017", "t1")

    logger.entry("stage1", [1, 2, 3], ["a", "b", "c"])
    logger.entry("stage2", [1, 2, 3], ["a", "b", "c"])
